
package org.gcube.common.authorization.control;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.gcube.common.authorization.control.annotations.AuthorizationControl;
import org.gcube.common.security.Owner;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class AuthorizationAspect {

	Logger log = LoggerFactory.getLogger(AuthorizationAspect.class);
	
	@Pointcut("@annotation(org.gcube.common.authorization.control.annotations.AuthorizationControl)")
	public void authorizationEntryPoint() {
	}
	
	
	@Pointcut("execution(* *.*(..))")
	public void anyCall() {
	}
	
	@Before("authorizationEntryPoint() && anyCall()")
	public void before(JoinPoint joinPoint) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
	    Method method = signature.getMethod();
		AuthorizationControl authAnn = (AuthorizationControl) method.getAnnotation(AuthorizationControl.class);
		log.info("aspect before with annotation {} allowed role {} and allowed users {} in method {}", authAnn.annotationType(), authAnn.allowedRoles(), authAnn.allowedUsers(), method.getName());

		Owner user = SecretManagerProvider.get().getOwner();
		
		String userId = user.getId();
		Collection<String> userRoles = user.getRoles();
		log.info("user role is {} and user {}", userRoles, userId );
		if (authAnn.allowedUsers().length!=0 && !Arrays.asList(authAnn.allowedUsers()).contains(userId)) {
			RuntimeException ex = authAnn.exception().getConstructor(Throwable.class).newInstance(new SecurityException(String.format("user %s not allowed to call method %s", userId, method.getName())));
			throw ex;	
		}
		List<String> allowedRoles = Arrays.asList(authAnn.allowedRoles());
		if (authAnn.allowedRoles().length!=0 && userRoles.stream().filter(i -> allowedRoles.contains(i)).collect(Collectors.toList()).isEmpty()) {
			RuntimeException ex = authAnn.exception().getConstructor(Throwable.class).newInstance(new SecurityException(String.format("user %s not allowed to call method %s (role not valid)", userId, method.getName())));
			throw ex;	
		}
	}
	
}
