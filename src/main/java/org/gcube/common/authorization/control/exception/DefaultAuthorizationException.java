package org.gcube.common.authorization.control.exception;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response.Status;

public class DefaultAuthorizationException extends WebApplicationException  {
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DefaultAuthorizationException(Throwable cause) {
		super(cause, Status.FORBIDDEN);
	}

}