# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.0]

- Moved to smartgears 4.0.0

## [v1.1.1] - 2018-12-03

- First commit